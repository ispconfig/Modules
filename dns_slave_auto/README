Author: Florian Schaal, schaal @it
        Tim de Boer, tim427

Intitial ISPConfig version: 3.1.3
Updated and tested on ISPConfig version: 3.2.8p2

This addon adds Secondary DNS-zone on additional servers after creating a DNS-zone
manually or via the wizard, or after updating, or deleting a DNS-zone.
If you managed multiple servers with ISPConfig but did not use the mirror-function,
you can i.e. use server 1 for bind and server 2 as a bind-slave. This plugin
manages the slave-zones on server 2 when you create / change / delete the
dns-zone on server 1.

Installation
--------------------

Download the addon to your server and run the install.sh script to install it. Take care to log out from ISPConfig and then log in again after installing to activate the plugin.


Usage
--------------------

Adjust the following settings in /usr/local/ispconfig/interface/lib/plugins/dns_dns_slave_auto_plugin.inc.php

1.
//* define your Nameserver IP(s) that the Secondary server should accept the data from (same as NS (IP-address) in Secondary DNS Zones)
$this->nameservers[] = '192.168.0.1';
$this->nameservers[] = 'fe80::2';

If your master-server has only one IP, just remove other lines and keep just one.

2.
//* define the server_ids for slave-server(s) (see server_id in dbispconfig.server on the master)
$this->dns_slave_server_id[] = 2;

If you want to run bind-slave on several servers, use more settings. I.e.
$this->dns_slave_server_id[] = 2;
$this->dns_slave_server_id[] = 12;
$this->dns_slave_server_id[] = 18;

If your have only one slave-server, just remove other lines and keep just one.

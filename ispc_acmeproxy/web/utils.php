<?php

/**
 * @param string $data
 * @param string $key
 * @param string $method
 * @return string
 */
function encrypt(string $data, string $key, string $method): string
{
    $ivSize = openssl_cipher_iv_length($method);
    $iv = openssl_random_pseudo_bytes($ivSize);

    $encrypted = openssl_encrypt($data, $method, $key, OPENSSL_RAW_DATA, $iv);

    // For storage/transmission, we simply concatenate the IV and cipher text
    //$encrypted = base64_encode($iv . $encrypted);
    $encrypted = bin2hex($iv . $encrypted);

    return $encrypted;
}

/**
 * @param string $data
 * @param string $key
 * @param string $method
 * @return string
 */
function decrypt(string $data, string $key, string $method): string
{
    //$data = base64_decode($data);
    $data = hex2bin($data);
    $ivSize = openssl_cipher_iv_length($method);
    $iv = substr($data, 0, $ivSize);
    $data = openssl_decrypt(substr($data, $ivSize), $method, $key, OPENSSL_RAW_DATA, $iv);

    return $data;
}

/**
 * @param $string
 * @param $endString
 * @return bool
 */
function endsWith($string, $endString)
{
    $len = strlen($endString);
    if ($len == 0) {
        return true;
    }
    return (substr($string, -$len) === $endString);
}

function jsonRequest($url, $data)
{
    $request = array(
        'http' => array(
            'method' => 'POST',
            'content' => json_encode($data),
            'header' =>
                "Content-Type: application/json\r\n" .
                "Accept: application/json\r\n"
        )
    );
    $context = stream_context_create($request);
    $result = file_get_contents($url, false, $context);
    $response = json_decode($result);
    return $response;
}

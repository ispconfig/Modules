### About this module

The experimental ispc_acmeproxy module is an attempt to create an environment running alongside an existing ISPConfig installation and uses normal user login credentials to edit DNS TXT records needed for the ACME DNS domain validation. It's primarily made for the [acme.sh](https://github.com/Neilpang/acme.sh) client. The ISPConfig DNS plugin for acme.sh can be used without changes, using user credentials instead of remoting api credentials that cannot be restricted to user resources. Experimental means it works fine for me where I need it, but probably needs more rigorous testing.


### Suggested usage

1. create an ISPC remoting user with proper rights (Client functions, DNS zone functions, DNS txt functions)
2. checkout the repository and copy the ispc_acmeproxy to a convenient place
3. create a new vhost under your web server and point the root to ispc_acmeproxy/web
4. copy ispc_acmeproxy/config/config.inc.php.sample to ispc_acmeproxy/config/config.inc.php and edit accordingly (use credentials from step 1)
5. reload your web server, test using the acme<span></span>.sh client, etc.

You'll need to provide user credentials instead of the API credentials for the DNS plugin as per [the docs](https://github.com/Neilpang/acme.sh/tree/master/dnsapi).


### Etc.

See this [forum thread](https://www.howtoforge.com/community/threads/restricting-api-users-to-certain-domains-or-limiting-to-one-clients-resources.80450/)

Suggestions, issue reports, PRs are welcome.

#### ISPConfig dns_dns_zone_disable_user_plugin

##### Description

This addon prevents non-admin users to **delete** or **change** DNS Zones in ISPConfig. If the user tries to delete a zone the following error will appear:


> You are not allowed to delete DNS Zones.

If the user tries to change DNS Zone settings the following error will appear:

> You are not allowed to change DNS Zonesettings.



##### Installation

Within the folder of this plugin you can find a `install.sh` script to automatically install the plugin within ISPConfig. However make sure to `chmod +x install.sh` beforehand.

You can also install the plugin manually with these steps:

```bash
mv -v dns_dns_zone_disable_user_plugin.inc.php /usr/local/ispconfig/interface/lib/plugins
chown ispconfig:ispconfig /usr/local/ispconfig/interface/lib/plugins/dns_dns_zone_disable_user_plugin.inc.php
```

**Note:** In both cases make sure to logout and back in to ISPConfig to load th Plugin. 



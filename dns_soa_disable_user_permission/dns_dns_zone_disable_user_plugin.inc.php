<?php
/**
 * dns_dns_zone_disable_user_plugin
 *
 * @author pyte_c
 */

class dns_dns_zone_disable_user_plugin {

  var $plugin_name = 'dns_dns_zone_disable_user_plugin';
  var $class_name = 'dns_dns_zone_disable_user_plugin';

  function onLoad() {
    global $app;
    // Register Events
    $app->plugin->registerEvent('dns:dns_soa:on_before_update', 'dns_dns_zone_disable_user_plugin', 'dns_dns_soa_edit');
    $app->plugin->registerEvent('dns:dns_soa:on_check_delete', 'dns_dns_zone_disable_user_plugin', 'dns_dns_soa_delete');
  }

  function dns_dns_soa_edit($event_name, $page_form) {
    global $app;
    $tmp = $app->db->diffrec($page_form->oldDataRecord, $app->tform->getDataRecord($page_form->id));

    if($tmp['diff_num'] > 0 && !$app->auth->is_admin()) {
      $app->error("You are not allowed to change DNS Zonesettings.");
    }
  }

  function dns_dns_soa_delete($event_name) {
    global $app;

    // If user is not admin and event is check_delete -> show error
    if($event_name == 'dns:dns_soa:on_check_delete' && !$app->auth->is_admin()) {
      $app->error("You are not allowed to delete DNS Zones.");
    }

  }
}

